<?php

namespace ModelApi;

/**
 * Description of Model
 *
 * @author kubrey
 */
class Model {
    
    protected $errors = array();


    public function __construct() {
        
    }
    
    /**
     * 
     * @return array
     */
    public function getErrors(){
        return $this->errors;
    }
    
    /**
     * 
     * @return string
     */
    public function getErrorsAsString(){
        return ($this->errors) ? implode('\n',$this->errors) : '';
    }
    
    /**
     * 
     * @param error $error
     */
    protected function setError($error){
        $this->errors[] = $error;
    }
    
    /**
     * 
     * @return system
     */
    public function getClassName(){
        return __CLASS__;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableName(){
        return '';
    }
    
    public function
    
    
}

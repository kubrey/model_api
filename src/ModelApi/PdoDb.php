<?php

namespace ModelApi;

use ModelApi\Db;

//use ModelApi\PdoStatement;

/**
 * Description of Dquery
 *
 * @author kubrey
 */
class PdoDb {

    protected static $instances = array();
    protected static $connections = array(
        'db1' => array('driver' => 'mysql', 'db' => 'backend', 'host' => 'localhost', 'username' => 'root', 'password' => 'dsotm1973')
    );

    /**
     * 
     * @param string $name
     * @return \ModelApi\Db
     */
    private function __construct($name) {

            self::$instances[$name] =  new Db(self::$connections[$name]['driver'] . ':dbname=' . self::$connections[$name]['db'] . ';host=' . self::$connections[$name]['host'], self::$connections[$name]['username'], self::$connections[$name]['password'],
                    array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"));
    }

    /**
     * 
     * @param string $name
     * @return \ModelApi\Db
     * @throws \Exception|\PDOException
     */
    public static function conn($name = "db1") {
        if (!array_key_exists($name, self::$instances)) {
            if (!array_key_exists($name, self::$connections)) {
                throw new Exception("No connections settings for the " . $name . " key");
            }
            $class = __CLASS__;
             new $class($name);
        }
        return self::$instances[$name];
    }

}

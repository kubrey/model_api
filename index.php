<?php

require_once './vendor/autoload.php';

require_once './src/ModelApi/Db.php';
require_once './src/ModelApi/PdoDb.php';
require_once './src/ModelApi/PdoStatement.php';

use ModelApi\PdoDb;

try {
    $data = PdoDb::conn()->query("show tables;");
    var_dump($data);
} catch (Exception $ex) {
    echo $ex->getMessage();
} catch (PDOException $ex) {
    echo $ex->getMessage();
}

